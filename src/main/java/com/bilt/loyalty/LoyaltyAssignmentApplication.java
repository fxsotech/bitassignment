package com.bilt.loyalty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoyaltyAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoyaltyAssignmentApplication.class, args);
	}

}
