package com.bilt.loyalty.repository;

import com.bilt.loyalty.model.AvailableProgram;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AvailableProgramRepository extends CrudRepository <AvailableProgram, Integer> {

    /*


    @Query(
            value = "SELECT case when count(*) > 0 then TRUE else FALSE end FROM available_program where mnem = ?mnem"
            ,nativeQuery = true)
    boolean existsByProgramMne(@Param("mnem") String mnem);

    @Query(
            value = "SELECT id FROM available_program where mnem = ?mnem"
            ,nativeQuery = true)
    Integer findIdByProgramMne(@Param ("mnem") String mnem);
*/


    @Query(
            value = "SELECT case when count(*) > 0 then TRUE else FALSE end FROM available_program where mnem = ?1"
            ,nativeQuery = true)
    boolean existsByProgramMnem(final String mnem);


    @Query(
            value = "SELECT id FROM available_program where mnem = ?1"
            ,nativeQuery = true)
    Integer findIdByProgramMnem(final String mnem);


}
