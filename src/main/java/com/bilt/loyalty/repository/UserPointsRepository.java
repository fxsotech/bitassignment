package com.bilt.loyalty.repository;

import com.bilt.loyalty.model.UserPoints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPointsRepository extends CrudRepository <UserPoints, Integer> {
}
