package com.bilt.loyalty.repository;

import com.bilt.loyalty.model.PointTransfer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointTransferRepository extends CrudRepository <PointTransfer, Integer> {
}
