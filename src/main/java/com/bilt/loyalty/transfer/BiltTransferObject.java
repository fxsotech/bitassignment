package com.bilt.loyalty.transfer;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

import java.io.Serializable;

@Getter @Setter
@Log

public class BiltTransferObject implements Serializable {

    private int biltPointSubmitted;
    private int programPointAwarded;
    private int biltPointUsed;
    private String program;
    private TransferStatus status;

    public BiltTransferObject() {}

    public BiltTransferObject (BiltTransferObject obj) {
        this.biltPointSubmitted = obj.biltPointSubmitted;
        this.programPointAwarded = obj.getProgramPointAwarded();
        this.biltPointUsed = obj.getBiltPointUsed();
        this.program = obj.program;
        this.status = TransferStatus.ACCEPTED;
    }
}
