package com.bilt.loyalty.transfer;

public enum TransferStatus {


    ACCEPTED ( "Transfer Accepted"), REJECTED ("Transfer Rejected"), IGNORED ("Transfer Ignore"), ERROR ("Transfer Error");
    private final String text;

    TransferStatus(String text) {
        this.text = text;
    }

    @Override
    public String toString()  {
        return this.text;
    }


};