package com.bilt.loyalty.transfer;


import lombok.extern.java.Log;

import org.springframework.validation.Errors;
import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

@Controller
@Log

public class Pseudo3PartyController {

    // BindException
    private Errors errors;




    // ASSUMPTION 1 Program Point = n BillPoint where n > 1
    @PostMapping("/pseudoservice/exchange")
    @ResponseBody

    protected BiltTransferObject exchange (
            @RequestBody BiltTransferObject req) {
        log.info ("Transfer processing....");
        BiltTransferObject rs = new BiltTransferObject(req);
        int biltPointSubmitted = req.getBiltPointSubmitted();
        int programPointBase = BiltPointValue.Conversion.getOrDefault(req.getProgram(), 0);
        log.info("Point submitted=" + biltPointSubmitted);
        log.info( "Program Conversion Rate=" + programPointBase);
        if  (biltPointSubmitted == 0 ) {
            rs.setStatus (TransferStatus.IGNORED);
        } else if  (programPointBase == 0) {
            req.setStatus(TransferStatus.ERROR);
        } else if ( programPointBase > biltPointSubmitted ) {
            // assumption 1 program point > 1 bilt point
           rs.setStatus(TransferStatus.REJECTED);
        } else {
                int pointAwarded = req.getBiltPointSubmitted() / programPointBase;
                if (pointAwarded == 0) {
                    rs.setStatus(TransferStatus.IGNORED);
                } else {
                    rs.setProgramPointAwarded(pointAwarded);
                    rs.setBiltPointUsed (biltPointSubmitted - (biltPointSubmitted % programPointBase));

                }
                rs.setStatus(TransferStatus.ACCEPTED);

        }

            log.info ("Transfer done");

        return rs;
}

}