package com.bilt.loyalty.service;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;

@Getter
@Setter
@Log
public class ClientServiceResponse {
    private int pointsAwarded;
    private int pointsUsed;
    private String message = "Welcome to BILT";

}
