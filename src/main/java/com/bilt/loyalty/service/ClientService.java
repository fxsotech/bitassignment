package com.bilt.loyalty.service;


import com.bilt.loyalty.transfer.BiltTransferObject;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Log
@Service
@PropertySource("classpath:application.properties")
public class ClientService {

    @Autowired
    RestTemplate restTemplate;

    @Value("${bilt.loyalty.points.transfer.service.url}")
    String uri;

    public ClientServiceResponse transferPoints(String program, int points) {
        BiltTransferObject xferObj = new BiltTransferObject();
        xferObj.setBiltPointSubmitted(points);
        xferObj.setProgram(program);
        RequestEntity request = RequestEntity
                .post(uri)
           //     .accept(MediaType.APPLICATION_JSON)
                .body(xferObj);
        ResponseEntity<BiltTransferObject> resp = restTemplate.exchange(request,BiltTransferObject.class);
        ClientServiceResponse csResp = new ClientServiceResponse();
        csResp.setPointsAwarded(resp.getBody().getProgramPointAwarded());
        csResp.setPointsUsed(resp.getBody().getBiltPointUsed());
        csResp.setMessage(resp.getBody().getStatus().toString());
        return csResp;
    }

}
