package com.bilt.loyalty.model;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_points")
@Getter @Setter @NoArgsConstructor

public class UserPoints {
    @Id
    private Integer id;
    private Integer idUser;
    private Integer idProgram;
    private Long totalPoints;
}
