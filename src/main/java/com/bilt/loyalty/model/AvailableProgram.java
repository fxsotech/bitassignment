package com.bilt.loyalty.model;

//   Available Loyalty Program

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

    @Entity
    @Table(name="available_program")
    @Getter @Setter @NoArgsConstructor
    public class AvailableProgram {

        @Id
        private Integer id;
        private String name;
        private String mnem;
        // Not Used except to show; calc is done in Service
        private Integer biltPointValue;
    }


