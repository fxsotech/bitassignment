package com.bilt.loyalty.model;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")
@Getter @Setter @NoArgsConstructor

public class User {
    @Id
    private Integer id;
    private String name;
}


