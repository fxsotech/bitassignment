package com.bilt.loyalty.model;


import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name="point_transfer")
@Getter @Setter @NoArgsConstructor

public class PointTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID",updatable = false, nullable = false)
    private Integer id;
    private Integer idUser;
    private Integer idProgram;
    private Integer biltPoints;
    private Integer programPoints;
    private Date dateOfExchange;

    public PointTransfer(Integer idUser, Integer idProgram,  Integer biltPoints, Integer programPoint) {
        this.setIdUser(idUser);
        this.setIdProgram(idProgram);
        this.setBiltPoints(biltPoints);
        this.setProgramPoints(programPoint);
        this.setDateOfExchange(Calendar.getInstance().getTime());
    }
}