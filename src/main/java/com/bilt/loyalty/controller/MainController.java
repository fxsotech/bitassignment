package com.bilt.loyalty.controller;

import com.bilt.loyalty.model.*;
import com.bilt.loyalty.repository.*;

import com.bilt.loyalty.service.ClientService;
import com.bilt.loyalty.service.ClientServiceResponse;
import com.bilt.loyalty.transfer.BiltTransferObject;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@Log
public class MainController {

    // BindException
    private Errors errors;

    @Autowired
    ClientService clientService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PointTransferRepository pointTransferRepository;

    @Autowired
    private AvailableProgramRepository availableProgramRepository;

    @GetMapping("/programs")
    public List<AvailableProgram> listPrograms() {
        List<AvailableProgram> list = new ArrayList<>();
        Iterable<AvailableProgram> iterable =  availableProgramRepository.findAll();
        Iterator<AvailableProgram>iter = iterable.iterator();
        iter.forEachRemaining(list::add);
        return list;
    }

    @GetMapping(value="/transfer")
    public String transferPoints(
            @RequestParam (name="account") Integer account,
            @RequestParam  (name="program") String progMnem,
            @RequestParam   (name="quantity") Integer points)
           {

        if ( ! userRepository.existsById(account) ) {
            return "Invalid Account";
        } else if ( ! availableProgramRepository.existsByProgramMnem(progMnem)) {
            return "Invalid Program Requested";
        } else if ( points == 0 ) {
            return "0 point requested";
        }
       Integer idProgram  = availableProgramRepository.findIdByProgramMnem(progMnem);
        AvailableProgram prog = (availableProgramRepository.findById(account)).get();

        ClientServiceResponse response = clientService.transferPoints(progMnem, points );

               int pointsUsed = response.getPointsUsed();
               int pointsAwarded = response.getPointsAwarded();
               int pointsReturned = points - pointsUsed;
        PointTransfer pt = new PointTransfer( account, idProgram, pointsUsed, pointsAwarded);

        pointTransferRepository.save(pt);
        return "Transfer Completed. Points Used: " + pointsUsed + " | Points Awarded: " + pointsAwarded
                + " | Points Returned: " + pointsReturned + " | " + response.getMessage();
    }

    @GetMapping("history")
    public List<PointTransfer> showHistory() {
        List<PointTransfer> list = new ArrayList<>();
        Iterable<PointTransfer> iterable =  pointTransferRepository.findAll();
        Iterator<PointTransfer>iter = iterable.iterator();
        iter.forEachRemaining(list::add);
        return list;
    }


/*

    @ExceptionHandler({Exception.class})
    public void handleException (Exception ex) {

        log.severe(ex.toString());
    }

*/


}