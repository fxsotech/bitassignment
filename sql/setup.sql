
--TABLE: avail_program;
create table available_program (id int, name varchar2(500), mnem varchar(5), bilt_point_value int);

insert into available_program values ( 1, 'United Airlines', 'UAL', 20);

insert into available_program values ( 2,  'Holiday Inn', 'HOLDY',10);

commit;

--TABLE: user;
insert into user values ( 1, 'Kung Foo');

insert into user values ( 2,  'Cliff Bar');

commit;

--TABLE: point_transfer
create table point_transfer ( id identity, id_user int, id_program int, bilt_points int, program_points int, date_of_exchange date);

